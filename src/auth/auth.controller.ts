import { Body, Controller, Delete, Get, Param, Post, Put, Req } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthDTO } from './dto/auth.dto';
import { Auth ,AuthSchema} from './schemas/auth.schemas';
const bcrypt = require("bcrypt");

@Controller('Users')
export class AuthController {
  constructor(private readonly auth_service: AuthService) {}

  @Post("/register")
  async create(@Body() authDTO: AuthDTO) {
    if(authDTO.username==null ||authDTO.password==null ||authDTO.name==null ||
      typeof(authDTO.username)!=typeof("") || typeof(authDTO.password)!=typeof("") || typeof(authDTO.name)!=typeof("")){
      return {"msg":"Username, password, name must be string."};
    }
    console.log(authDTO.username);
    if (authDTO.username.length<3 || authDTO.username.length>20){
      return {"msg":"3 < Username's length < 20"};
    }
    console.log("Checkpoint");
    if (authDTO.name.length<3 || authDTO.name.length>20){
      return {"msg":"3<name's length <20"};
    }
    if (authDTO.password.length<3 || authDTO.password.length>20){
      return {"msg":"3<password's length <20"};
    }
 

    authDTO.create_time = Date.now();
    const result =  await this.auth_service.findOne(null,authDTO.username);
    if (result == null){
      const salt = await bcrypt.genSalt(10);
      authDTO.password = await bcrypt.hash(authDTO.password, salt);
      await this.auth_service.create(authDTO);
      return {"msg":"Create user: ["+authDTO.username+"]"};
    }else{
      return {"msg":"User: ["+authDTO.username+"] exists"};
    }
  }

  @Post("/login")
  async login(@Body() authDTO: AuthDTO) {
    try{
      const ret_val = this.auth_service.login(authDTO);
      return ret_val;
    }
    catch(ex){
      return {"msg":"Username or password not correnct."}
    }
  }

  @Put("/delete")
  async delete(@Body() authDTO: AuthDTO) {
    const db_authen_dto = await this.auth_service.findOne(null,null,authDTO.token);
    if (db_authen_dto==null){
      return {"msg":"Plase login."}
    }
    if (db_authen_dto.username==authDTO.username){
      return {"msg":"Can not delete current user."}
    }
    if (await this.auth_service.findOne(null,authDTO.username,null)==null){
      return {"msg":"Username "+authDTO.username+" not found."}
    }
    if (this.auth_service.deleteOne(null,authDTO.username)){
      if (await this.auth_service.findOne(null,authDTO.username)!=null){
        return {"msg":"Delete "+authDTO.username+" failed."}
      }
      return {"msg":"Delete "+authDTO.username+" successfull"}
    }else{
      return {"msg":"Delete "+authDTO.username+" failed."}
    }

  }
  @Post("/logout")
  async logout(@Body() authDTO: AuthDTO) {
    const db_authen_dto = await this.auth_service.findOne(null,null,authDTO.token);
    if (db_authen_dto==null){
      return {"msg":"Plase login."}
    }
    const saltOrRounds = 10;
    db_authen_dto.token = await bcrypt.hash(String(Date.now()), saltOrRounds);
    console.log("Token: "+db_authen_dto.token)
    const result = await this.auth_service.updateOne(db_authen_dto.username,{"token":db_authen_dto.token});
    return {"msg":""+db_authen_dto.username+" logout successfull."}
  }
}