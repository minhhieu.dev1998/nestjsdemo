import { Injectable, ForbiddenException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { AuthDTO } from './dto/auth.dto';
import { Auth, AuthDocument } from './schemas/auth.schemas';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthService {
  constructor(
    @InjectModel(Auth.name) private readonly authModel: Model<AuthDocument>,
  ) { }

  async create(authDTO: AuthDTO): Promise<Auth> {
    const createdUser = await this.authModel.create(authDTO);
    return createdUser;
  }


  async login(authDTO: AuthDTO) {
    // find the user by user
    const user = await this.findOne(null, authDTO.username);
    console.log(user == null)
    // if user does not exist throw exception
    if (user == null) {
      return { "msg": "Username or password not correnct." };
    }
    // compare password
    const isMatch = await bcrypt.compare(authDTO.password, user.password);
    // if password incorrect throw exception
    console.log(isMatch)
    if (!isMatch) {
      return { "msg": "Username or password not correnct." };
    }
    const saltOrRounds = 10;
    authDTO.token = await bcrypt.hash(String(Date.now()), saltOrRounds);
    const result = await this.updateOne(authDTO.username,{"token":authDTO.token});
    if (result) {
      return { "msg": "login successfull", "token": authDTO.token }
    } else {
      return { "msg": "login failed" }
    }
  }

  async updateOne(username: string,update:object): Promise<boolean> {
    const query = { "username": username };
    try {
      const retVal = await this.authModel.updateOne(query,update);
    } catch (ex) {
      return false;
    }
    return true;
  }
  async findOne(id: string = null, username: string = null,token:string=null): Promise<Auth> {
    const query = {};
    if (id != null) {
      query["_id"] = id;
    } else if (username != null) {
      query["username"] = username;
    } 
    else if (token != null) {
      query["token"] = token;
    } else {
      query["_id"] = null;
    }
    console.log("Query "+JSON.stringify(query) )
    const retVal = await this.authModel.findOne(query);
    console.log(retVal)
    return retVal;
  }

  async deleteOne(id: string = null, username: string = null): Promise<boolean> {
    const query = {};
    if (id != null) {
      query["_id"] = id;
    } else if (username != null) {
      query["username"] = username;
    } else {
      query["_id"] = null;
    }

    try {
      const retVal = await this.authModel.deleteOne(query);
    } catch (ex) {
      return false;
    }
    return true;
  }
  
}