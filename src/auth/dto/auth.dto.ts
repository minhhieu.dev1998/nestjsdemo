import {
    IsNotEmpty,
    IsString,
  } from 'class-validator';


export class AuthDTO {
    @IsString()
    @IsNotEmpty()
    readonly username: string;
    
    @IsString()
    @IsNotEmpty()
    password: string;
    
    create_time: number;
    token: string;
    
    @IsString()
    @IsNotEmpty()
    readonly name: string;
}